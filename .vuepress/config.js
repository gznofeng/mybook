module.exports = {
  title: 'ODS业务文档',
  description: '专注于ODS业务的入门学习文档',
  themeConfig: {
    nav: [
      { text: '主页', link: '/' },
      { text: '业务相关', link: '/business/business.md' },
      { text: '架构相关', link: '/arch/' },
    ],
	sidebar: 'auto'
  }

}