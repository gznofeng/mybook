# 常用的代码片段

### listanymatch
> 对比集合中是否有符合条件的对像
```java
boolean isAnyMach=ListUtils.emptyIfNull($VAR$).stream().anyMatch(l->{
			//对比逻辑填这里
			return true;
		});
```

### listnull
> 如果为空时创建空集合
```java
ListUtils.emptyIfNull($VAR$)
```

### listomap
> 集合转换为MAP
```java
Map map=ListUtils.emptyIfNull($VAR$).stream().collect(Collectors.toMap(k->k, v->v,(v1,v2)->v2));
```

### listtransfrom
> 集合转换为新对像
```java
List<> resultList=Lists.transform(ListUtils.emptyIfNull(list),l->{
			//把list中的对像转为另外的对像
			return l;
		});
```

### listvotolistid
> 从集合中取得指定值
```java
List<String> snList=ListUtils.emptyIfNull($VAR$).stream().map(l->l.getOrderSn()).collect(Collectors.toList());
```

### notnull
> 对像非空判断
```java
Objects.nonNull($VAR$)
```


### notnull_mutile
> 多层非空判断
```java
Optional.ofNullable($VAR$).map(h->h.getDetailList()).orElse(null);
```

### set_unmodify
> 定义不可修改的SET
​```java
public static final Set<Integer> UNMODIFY_SET = ImmutableSet.of(1,2,3);
```

### listfirst
> 取集合中的第一个对像，无值默认为空.FluentIterable.from不能为null
​```java
FluentIterable.from(CollectionUtils.emptyIfNull($VAR$)).first().orNull()
```

### map_unmodify
> 定义不可修改的MAP
​```java
public static final ImmutableMap immutableMap=ImmutableMap.builder().put(1,2).put(2,1).build();
```

