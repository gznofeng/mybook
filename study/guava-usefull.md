# 概括
Lists
    Lists.newArrayList


    public static <E> ArrayList<E> newArrayList(E... elements) {
        Preconditions.checkNotNull(elements);
        int capacity = computeArrayListCapacity(elements.length);
        ArrayList<E> list = new ArrayList(capacity);
        Collections.addAll(list, elements);
        return list;
    }

     @VisibleForTesting
    static int computeArrayListCapacity(int arraySize) {
        CollectPreconditions.checkNonnegative(arraySize, "arraySize");
        return Ints.saturatedCast(5L + (long)arraySize + (long)(arraySize / 10));
    }

    >当使用Lists.newArrayList时，guava会优先定义一个arrayList长度为5+X+X/10的初始化大小size。


    public static <F, T> List<T> transform(List<F> fromList, Function<? super F, ? extends T> function) {
        return (List)(fromList instanceof RandomAccess?new Lists.TransformingRandomAccessList(fromList, function):new Lists.TransformingSequentialList(fromList, function));
    }

     private static class TransformingRandomAccessList<F, T> extends AbstractList<T> implements RandomAccess, Serializable {
        final List<F> fromList;
        final Function<? super F, ? extends T> function;
        private static final long serialVersionUID = 0L;

        TransformingRandomAccessList(List<F> fromList, Function<? super F, ? extends T> function) {
            this.fromList = (List)Preconditions.checkNotNull(fromList);
            this.function = (Function)Preconditions.checkNotNull(function);
        }

        public void clear() {
            this.fromList.clear();
        }

        public T get(int index) {
            return this.function.apply(this.fromList.get(index));
        }

        public Iterator<T> iterator() {
            return this.listIterator();
        }

        public ListIterator<T> listIterator(int index) {
            return new TransformedListIterator<F, T>(this.fromList.listIterator(index)) {
                T transform(F from) {
                    return TransformingRandomAccessList.this.function.apply(from);
                }
            };
        }

        public boolean isEmpty() {
            return this.fromList.isEmpty();
        }

        public T remove(int index) {
            return this.function.apply(this.fromList.remove(index));
        }

        public int size() {
            return this.fromList.size();
        }
    }

    >当使用transform的时候，实现的方式是返TransformingRandomAccessList。在TransformingRandomAccessList客户重写了get方法，在调用get方法时调用Function中的appaly方法.需要注意的是如果使用transfrom一定不要以为是返回了一个转换出来的新对像，实际只是对原List进行了方法封装。

    public static void main(String[] args) {
		List<Integer> ints = Lists.newArrayList(1,2,3,4,5);
		List<Integer> transfromL=Lists.transform(ints, new Function<Integer, Integer>() {
			@Override
			public Integer apply(@Nullable Integer integer) {
				return integer;
			}
		});
		System.out.println("转换后的结果长度为:"+transfromL.size());
		ints.remove(0);
		System.out.println("对原始对像进行了remove后，转换结果的长度:"+transfromL.size());

	}























Maps
    Maps.newHashMap

    Map<String, Integer> map =new HashMap<String, Integer>(){{
			put("a",1);put("b",2);
		}};
		Function<Integer, Double> sqrt =
				new Function<Integer, Double>() {
					@Override
					public Double apply(Integer in) {
						return Double.valueOf(in-1);
					}
				};
		Map<String, Double> transformed = Maps.transformValues(map, sqrt);
		System.out.println(transformed);
		map.put("v",100);
		System.out.println(transformed);

    >当使用transformValues的时候，实际返回的是一个viewer对像而非新建对像，在调用get方法时调用Function中的appaly方法.



Preconditions
    Preconditions.checkNotNull
    Preconditions.checkState
Strings
    Strings.nullToEmpty
    Strings.emptyToNull
    Strings.lenientFormat("我爱%s,%s不爱我","他",1,2) 
    >改善了String.format中如果%s出现的数量小于传参时会抛出java.util.MissingFormatArgumentException: Format specifier '%s'的问题
ImmutableMap
    ImmutableMap.<String, String> builder().put
    >ImmutableMap 是一个不能修改的Map

HashMultimap与ArrayListMultimap
    >主要区别在于，HashMultimap实际在VALUE存储时使用HashSet,ArrayListMultimap使用ArrayList。

Optional
    >Optional的主要作用在于提高代码的可阅读性，强制了程序员要求对null对像的处理。
    >可以使用or设置默认值，forexample : Optional.ofNullable(a).orElse("a")

Splitter
    Splitter.on(",").trimResults().splitToList[把字符串转为List]
Joiner


LoadingCache
    >核心存储是Segment，segments中存放着多个Segment。在Segment中存在4个重要的队列分别是ReferenceQueue<K> keyReferenceQueue和ReferenceQueue<V> valueReferenceQueue，Queue<ReferenceEntry<K, V>> writeQueue和Queue<ReferenceEntry<K, V>> accessQueue
    一个table,AtomicReferenceArray。（AtomicReferenceArray是什么，能够做什么）
    一个this.threshold = newTable.length() * 3 / 4;（设置这个门槛的作用是什么？）

    put数据的时候发生过什么？
    1.把key值进行处理，实际使用的是guava中的Equivalence对像封装了hash的算法，默认使用object.hashcode().
    2.ReentrantLock lock(ReentrantLock的作用是什么？可以做什么呢)
    
 expireAfterWrite和 refreshAfterWrite结合使用.   

                 expireAfterWrite 的弊端:   如果缓存对象的过期时间非常接近，会造成大量对象集体过期.  在用cache.get 访问的时候，过期的key会调用 cacheLoader.load方法.  load 方法是线程安全的，而且是单线程的. 在一个key的值被load出来之前，其他的key的访问需要等待. 会导致耗时比单纯的不用缓存的时间还长.   所以 refreshAfterWrite 定时刷新就非常有用处.   例如 我们制定  refreshAfterWrite  为10 分钟， expireAfterWrite  为15分钟 .  那么在第 10-15分钟直接被访问的key就会触发刷新功能.    cache 会用之前的旧值先返回结果,之后再异步并发的去调用load方法去刷新key的值.   在新的值返回之前都使用旧值.   这样子既可以定时刷新，也可以强制过期刷新.  

https://www.cnblogs.com/aspirant/p/11734918.html


RateLimiter
RateLimiter.create(5)和RateLimiter.create(5, 1000,  TimeUnit.MILLISECONDS);的区别：
平滑突发限流(SmoothBursty)和平滑预热限流(SmoothWarmingUp)
什么叫平滑预热限流
速率是 梯形上升 速率的，也就是说 冷启动 时会以一个比较大的速率慢慢到平均速率；然后趋于 平均速率（梯形下降到平均速率）

https://www.jianshu.com/p/efac2713eb64

    >tryAcquire不会线程等待，会优先检查是否可能获得锁，如果不能获得命牌会直接返回FALSE代表当前获取命牌失败，否则会继续向下执行获取命牌。建议使用tryAcquire，和合理配置超时时间。
    >acquire 会线程阻塞，并返回等待时间。问题来了，如果有大量并发，而且都线程阻塞的话后果会非常严重。我们接下来要看一下RateLimiter将会怎么处理这种情况。使用了TimeUnit.NANOSECONDS.sleep(remainingNanos);进行了线程的阻塞，并没有限制最大值，如果真的在大并发的情况下，很容易会爆线程造成线程的阻塞，所以不建议使用acquire。
       @CanIgnoreReturnValue
        public double acquire(int permits) {
            long microsToWait = this.reserve(permits);
            this.stopwatch.sleepMicrosUninterruptibly(microsToWait);
            return 1.0D * (double)microsToWait / (double)TimeUnit.SECONDS.toMicros(1L);
        }

获取token流程：
acquire方法会获取指定token数，并返回等待时间。
第一次获取，不管获取多少，等待时间为0
可以提前消费token，即获取token数超过storedPermits，storedPermits会被置0，然后把 nextFreeTicketMicros 设置到下一次可用时刻。
获取token时，假如还没到nextFreeTicketMicros ，则一直sleep到该时刻再返回等待时间。
每次获取时，检查nextFreeTicketMicros ，如果是小于当前时间，则补上这段时间的token数，加到storedPermits中。
tryAcquire主要不同是会判断timeout时间是否在nextFreeTicketMicros之前，不是的话直接返回false。是的话等到nextFreeTicketMicros后返回true。

 

> 简单用1-2句话去描述你想要介绍的是什么？为什么想要做这个介绍（场景）。这个介绍能够为你带来什么?
> 例句：在实际的开发过程中，我们经常需要用到缓存。使用缓存常见的一个场景就是key不在缓存中，这个时候我们会去读取这个key对应的值，然后把这个值放到缓存中。对这种场景guava的loadingcache就给了比较好的实践，下面我们将会深入了解一下lodingcache的实现原理和代码实现。

> 详细介绍一下你对XXX的理解，在你的理解中你有什么特殊的个人见解。

# XXX介绍
# XXX使用用例
# XXX整体结构
# XXX细化分析
# XXX细化总结