# 概括

> 简单用1-2句话去描述你想要介绍的是什么？为什么想要做这个介绍（场景）。这个介绍能够为你带来什么?
> 例句：在实际的开发过程中，我们经常需要用到缓存。使用缓存常见的一个场景就是key不在缓存中，这个时候我们会去读取这个key对应的值，然后把这个值放到缓存中。对这种场景guava的loadingcache就给了比较好的实践，下面我们将会深入了解一下lodingcache的实现原理和代码实现。

> 详细介绍一下你对XXX的理解，在你的理解中你有什么特殊的个人见解。

# XXX介绍
# XXX使用用例
> 简单使用用例
org.apache.commons.lang3.StringUtils

isEmpty 和isBlank的区别，isEmpty简单粗暴，直接判断是否null和长度不>1,isBlank去空格后判断。简单点理解为empty就是判断什么都没有，而isBlank是否为空（含有空格）。

org.apache.commons.collections4.CollectionUtils

org.apache.commons.lang3.time.StopWatch
org.apache.commons.lang3.RandomStringUtils
org.apache.commons.collections4.SetUtil
org.apache.commons.lang3.time.DateFormatUtils
org.apache.commons.lang.builder.ToStringBuilder

org.apache.commons.collections.map.HashedMap


> 使用注意事项和参数
> 各参数的使用含义
# XXX在应用中可能遇到的问题及解决方案
> 使用了XXX，需要注意XXX，我们应该XXX，最佳的实践推荐。如果你需要XX，你可以
# XXX整体结构
# XXX细化分析
> 记录在过程中提出的疑问，并记录下对应的答案或讨论/思考的经过。最重要是要有结果
# XXX细化总结