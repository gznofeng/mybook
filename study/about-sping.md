# AbstractPlatformTransactionManager

setNestedTransactionAllowed(true);

TransactionAwareDataSourceProxy



# AbstractRoutingDataSource


CopyOnWriteArraySet?

#怎么通过代码配置拦截器




# about util
# ExpressionParser
# StandardEvaluationContext
# LocalVariableTableParameterNameDiscoverer

获得真实方法
public static Method getRealMethod(MethodInvocation invocation) {
		Class<?> targetClass  = AopUtils.getTargetClass(invocation.getThis()) ;
		Method specificMethod = ClassUtils.getMostSpecificMethod( invocation.getMethod(), targetClass);
		specificMethod = BridgeMethodResolver.findBridgedMethod(specificMethod);
		return specificMethod;
	}

获得方法中的参数
private ParameterNameDiscoverer paraNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();
paraNameDiscoverer.getParameterNames(realMethod);

根据条件解释出对应的对像，如#userId,即找到方法入参中的Long userId的值
public static Object getSpelValue(Object[] args, String[] paraNames, String key, BeanFactory beanFactory) {
		Assert.hasText(key);
	ExpressionParser ep = new SpelExpressionParser();
		StandardEvaluationContext context = new StandardEvaluationContext();
		if (beanFactory != null) {
			context.setBeanResolver(new BeanFactoryResolver(beanFactory));
		}
		if (!ArrayUtils.isEmpty(args) && !ArrayUtils.isEmpty(paraNames)) {
			if (args.length != paraNames.length) {
				throw new IllegalArgumentException("args length must be equal to paraNames length");
			}
			for (int i = 0; i < paraNames.length; i++) {
				context.setVariable(paraNames[i], args[i]);
			}
		}
		return ep.parseExpression(key).getValue(context);
}